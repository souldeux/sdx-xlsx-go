package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/tealeg/xlsx"
)

func timeit() func() {
	start := time.Now()
	return func() {
		log.Printf("Function runtime was %v\n", time.Since(start))
	}
}

func parse(path string) []byte {
	defer timeit()()

	results := make(map[string][]map[string]string)
	headerRow := make(map[string]string)

	xlFile, err := xlsx.OpenFile(path)
	if err != nil {
		panic(err)
	}
	for _, sheet := range xlFile.Sheets {
		var sheetResults []map[string]string

		for rowindex, row := range sheet.Rows {
			// Each row is a mapping of column identifiers (A, B, C, etc) to
			// the values contained in that column for the given row
			rowMap := make(map[string]string)

			for cellIndex, cell := range row.Cells {
				cellValue := cell.String()
				colIdentifier := string('A' + cellIndex)

				if rowindex == 0 {
					// cache header row
					headerRow[colIdentifier] = cellValue
				} else {
					prettyIdentifier := headerRow[colIdentifier]
					rowMap[prettyIdentifier] = cellValue
				}
			}

			if rowindex != 0 {
				sheetResults = append(sheetResults, rowMap)
			}
		}
		results[sheet.Name] = sheetResults
	}
	b, err := json.Marshal(results)
	if err != nil {
		panic(err)
	}
	return b
}

func uploadHandler(w http.ResponseWriter, r *http.Request) {
	file, header, err := r.FormFile("file")
	defer file.Close()

	if err != nil {
		panic(err)
	}

	out, err := os.Create("/tmp/file")
	if err != nil {
		panic(err)
	}
	defer out.Close()

	_, err = io.Copy(out, file)
	if err != nil {
		panic(err)
	}

	parseData := parse(out.Name())

	log.Printf("File %s was uploaded and parsed", header.Filename)
	log.Printf("Parse data: %s", parseData)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(parseData)
}

func healthcheckHandler(w http.ResponseWriter, r *http.Request) {
	log.Print("Health check invoked")
	fmt.Fprintf(w, "ok")
}

func main() {
	http.HandleFunc("/", healthcheckHandler)
	http.HandleFunc("/upload", uploadHandler)

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	log.Printf("Starting HTTP server on port %s", port)

	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), nil))
}
