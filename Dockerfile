FROM golang:alpine as builder
RUN apk --no-cache --update add git

WORKDIR /go/src/sdx/xlsx
COPY ./sdx-xlsx.go ./sdx-xlsx.go

RUN go get -v -t \
    github.com/tealeg/xlsx

RUN CGO_ENABLED=0 GOOS=linux go build -v -o sdx-xlsx

FROM alpine
RUN apk add --no-cache ca-certificates
COPY --from=builder /go/src/sdx/xlsx/sdx-xlsx /sdx-xlsx

CMD ["/sdx-xlsx"]