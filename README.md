# SDX-XLSX

A simple, fragile, Google-Cloud-Run-ready XLSX parsing microservice written in Go. Leverages the excellent [`tealeg/xlsx`](https://github.com/tealeg/xlsx) library.

## Usage

Run locally:

```bash
docker build -t sdx-xlsx .
docker run -p 80:8080 sdx-xlsx
```

Alternatively, build and deploy to Cloud Run:

```bash
gcloud builds submit --tag gcr.io/your-project-id/sdx-xlsx
gcloud beta run deploy sdx-xlsx --image gcr.io/your-project-id/sdx-xlsx --memory 256Mi
```

Be sure to answer "yes" when asked if you want to allow unauthenticated invocations.

Wherever you decide to run the thing, you can `POST` an XLSX file to the resultant `/upload` endpoint and get back a JSON representation of the data contained therein:

```bash
curl -X POST \
  https://your-google-cloud-run-address.app/upload \
  -F file=@/path/to/your.xlsx
```

You should receive a JSON object containing an array of "rows" for each sheet in the XLSX workbook. For instance, uploading the included `demo.xlsx` file will give you the following:

```json
{
    "Sheet1": [
        {
            "Email": "bob@example.com",
            "First Name": "Bob",
            "Last Name": "Jones"
        },
        {
            "Email": "sally@example.com",
            "First Name": "Sally",
            "Last Name": "Smith"
        },
        {
            "Email": "rosco@example.com",
            "First Name": "Rosco",
            "Last Name": "The Dog"
        }
    ],
    "Sheet2": [
        {
            "Alpha": "One",
            "Beta": "Two",
            "Charlie": "Three",
            "Delta": "Four"
        },
        {
            "Alpha": "Five",
            "Beta": "Six",
            "Charlie": "Seven",
            "Delta": "Eight"
        }
    ]
}
```

Note that for now this can really only parse well-formed data. For instance, we always consider the first row to be the header row. If that row is empty, things get weird. Things also get weird when data rows contain more filled cells than header rows.
